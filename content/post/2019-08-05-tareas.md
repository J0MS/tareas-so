---
title: Tareas
subtitle: Sistemas Operativos 2020-1
date: 2019-08-05
tags: ["tareas"]
---

En este espacio estaremos concentrando las tareas de la materia de [sistemas operativos][gitlab-so] semestre 2020-1 impartida en la [Facultad de Ciencias, UNAM][fciencias-unam].


[gitlab-so]: http://SistemasOperativos-Ciencias-UNAM.gitlab.io/
[fciencias-unam]: http://www.fciencias.unam.mx/docencia/horarios/20201/1556/713

